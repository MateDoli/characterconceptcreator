﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCCEnums {}

public enum Races
{
    // Base Races
    Dragonborn,
    Dwarf,
    Elf,
    Gnome,
    HalfElf,
    Halfling,
    HalfOrc,
    Human,
    Tiefling,
    
    // Eberron Races
    Changeling,
    Kalashtar,
    Shifter,
    Warforged,
    
    //Elemental Evil's Races
    Aarakocra,
    Genasi,
    Gloiath,
    
    // Guildmater's Guide Races
    Centaur,
    Loxodon,
    Minotaur,
    SimicHybrid,
    Vedalken,
    
    // Plane Shifts
    Aetherborn,
    Aven,
    Khenra,
    Kor,
    Merfolk,
    Naga,
    Siren,
    Vampire,
    
    // Volo's Guide Races
    Aasimar,
    Bugbear,
    Firbolg,
    Goblin,
    Hobgoblin,
    Kenku,
    Kobold,
    Lizardfolk,
    Orc,
    Tabaxi,
    Triton,
    YuanTiPureBlood,
    
    // Other
    Gith,
    Locathah,
    Tortle,
    Verdan
}

public enum Classes
{
    Artificer,
    Barbarian,
    Bard,
    Cleric,
    Druid,
    Fighter,
    Monk,
    Paladin,
    Ranger,
    Rogue,
    Sorcerer,
    Warlock,
    Wizard
}
