﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;



public class Card : MonoBehaviour
{
    [SerializeField]
    private Image classImage;
    [SerializeField]
    private TextMeshProUGUI className;
    [SerializeField]
    private TextMeshProUGUI classRoles;

    private CardType _cardType;

    private CardSO _dataItem;
    public void UpdateCardContent(CardSO dataItem)
    {
        if(!dataItem) return;
        classImage.sprite = dataItem.cardType == CardType.Class ? 
            InfoLibrary.instance.GetImage(MenuManager.Instance._chosenInfoData.CharacterRace, dataItem.currentClass) : dataItem.cardImage;
        className.text = dataItem.cardType == CardType.Race ? dataItem.race.ToString() : className.text = dataItem.currentClass.ToString();
        classRoles.text = dataItem.cardDescription;
        _cardType = dataItem.cardType;
        
        _dataItem = dataItem;
    }

    public void OnCardPress()
    {
        if(_cardType == CardType.Race)
        {
            MenuManager.Instance.ShowClassMenu(_dataItem);
        }

        if (_cardType == CardType.Class)
        {
            MenuManager.Instance.ShowCharacterInfoMenu(_dataItem);
        }
    }


    private void Start() => GetComponent<Button>().onClick.AddListener(OnCardPress);
}

public enum CardType
{
    Race, Class
}