﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using TMPro.Examples;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "Info", menuName = "Scriptable Objects/Class Information", order = 0)]
    public class CharacterDescriptionSO : ScriptableObject
    {
        public CardType infoType;

        [ShowIf(nameof(infoType), CardType.Race)]
        public Races characterRace;
        
        [ShowIf(nameof(infoType), CardType.Class)]
        public Classes characterClass;

        [TextArea] public string description;

        [TextArea] [ShowIf(nameof(infoType), CardType.Class)] public string skills;
        [TextArea] [ShowIf(nameof(infoType), CardType.Class)] public string tools;
    }
}