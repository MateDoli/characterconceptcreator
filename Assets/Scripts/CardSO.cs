﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Card", menuName = "Scriptable Objects/Card", order = 0)]
public class CardSO : ScriptableObject {

    public CardType cardType;
    
    [ShowIf(nameof(cardType), CardType.Race)]
    public Sprite cardImage;

    public string cardDescription;

    [ShowIf(nameof(cardType), CardType.Race)]
    public Races race;
    [ShowIf(nameof(cardType), CardType.Class)]
    public Classes currentClass;
    
    [ShowIf(nameof(cardType), CardType.Race)]
    public List<Classes> classes;
}

/*
 
 Race: Human ^^
 Class: Wizard ^^
 Description: idfpaisdnpfijzxpcivjx
 sdflainspcviz
 a
 sd
 fa
 sd
 fa
 df
 as
 d
 fa
 d
 fa
 sd
 fa
 df
 adf
 as
 df
  
  ^^
  

 
 





*/
