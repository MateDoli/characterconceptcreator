﻿
using System;
using System.Collections.Generic;
using DefaultNamespace;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu(menuName = "Library", fileName = "Library")]
public class InfoLibrary : ScriptableSingleton<InfoLibrary>
{
    private const string AssetPath = "Assets/Images/CharacterImages";
    
    public List<ImageItemConfig> items;
    public List<CharacterDescriptionSO> infos;
    public void GenerateList()
    {
        var data = AssetDatabase.FindAssets("t:texture2D", new[] {AssetPath});
        
        foreach (var VARIABLE in data)
        {
            var path = AssetDatabase.GUIDToAssetPath(VARIABLE);
            
            var index = path.LastIndexOf('/') + 1;
            var fullString = path.Substring(index);

            var strings = fullString.Split('_');

            var raceName = strings[0];
            var className = strings[1].Substring(0, strings[1].IndexOf('.'));

            var sprite = (Sprite)AssetDatabase.LoadAssetAtPath(path, typeof(Sprite));
            Enum.TryParse(raceName, out Races characterRace);
            Enum.TryParse(className, out Classes characterClass);
            
            
            var config = new ImageItemConfig(characterRace, characterClass, sprite);
            items.Add(config);
        }
    }

    public Sprite GetImage(Races ofRace, Classes ofClass) => items.Find(x => x.characterRace == ofRace && x.characterClass == ofClass).sprite;

    public string GetRaceDescription(Races ofRace) =>
        infos.Find(x => x.infoType == CardType.Race && x.characterRace == ofRace).description;

    public ClassInformation GetClassInformation(Classes ofClass)
    {
        CharacterDescriptionSO characterInformation =
            infos.Find(x => x.infoType == CardType.Class && x.characterClass == ofClass);

        return new ClassInformation(characterInformation);
    }
    

}

// Race_Class

[CustomEditor(typeof(InfoLibrary))]
public class InfoLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Generate List"))
        {
            InfoLibrary.instance.GenerateList();
        }
    }
}

[System.Serializable]
public class ImageItemConfig
{
    public ImageItemConfig(Races newRace, Classes newClass, Sprite newSprite)
    {
        characterRace = newRace;
        characterClass = newClass;
        sprite = newSprite;
    }
    
    public Races characterRace;
    public Classes characterClass;
    
    public Sprite sprite;
}

public readonly struct ClassInformation
{
    public string description { get; }
    public string tools { get; }
    public string skills { get; }

    public ClassInformation(CharacterDescriptionSO characterDescription)
    {
        description = characterDescription.description;
        tools = characterDescription.tools;
        skills = characterDescription.skills;
    }
}
