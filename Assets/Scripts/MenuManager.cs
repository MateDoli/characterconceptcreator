﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private static MenuManager _instance;

    public static MenuManager Instance => _instance;

    private void Awake()
    {
        _instance = this;
    }
    
    public GameObject raceMenu;
    public GameObject startButton;
    public Button resetButton;
    public ClassMenu classMenu;
    [FormerlySerializedAs("classInformationManager")] public CharacterInformationManager characterInformationManager;

    public ChosenInfoData _chosenInfoData;
    public void ShowRaceMenu()
    {
        startButton.SetActive(false);
        raceMenu.SetActive(true);
        raceMenu.GetComponent<CardMenu>().UpdateMenuContent();
        
    }

    public void ShowClassMenu(CardSO dataItem)
    {
        resetButton.gameObject.SetActive(true);
        raceMenu.SetActive(false);
        classMenu.gameObject.SetActive(true);
        _chosenInfoData = new ChosenInfoData();
        _chosenInfoData.CharacterRace = dataItem.race;
        classMenu.UpdateMenuContent(dataItem);
    }

    public void ShowCharacterInfoMenu(CardSO dataItem)
    {
        _chosenInfoData.CharacterClass = dataItem.currentClass;
        classMenu.gameObject.SetActive(false);
        characterInformationManager.gameObject.SetActive(true);
        characterInformationManager.SetAndUpdateData(_chosenInfoData);
    }

    public void ResetCharacter()
    {
        raceMenu.SetActive(true);
        classMenu.gameObject.SetActive(false);
        
        characterInformationManager.gameObject.SetActive(false);
    }
}

public struct ChosenInfoData
{
    public Races CharacterRace;
    public Classes CharacterClass;

    public ChosenInfoData(Races race, Classes newClass)
    {
        CharacterRace = race;
        CharacterClass = newClass;
    }
}



