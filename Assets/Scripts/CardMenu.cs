﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


public class CardMenu : MonoBehaviour
{
    
    public CardSO[] cards;

    public GameObject content;

    public GameObject cardPrefab;

    public virtual void UpdateMenuContent()
    {
        
        if(!cardPrefab) return;

        for(int i = content.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(content.transform.GetChild(i).gameObject);
        }
    }
    
    

    protected void SpawnCard(CardSO card)
    {
        GameObject newObject = Instantiate(cardPrefab, transform.position, Quaternion.identity);
        Card cardObject = newObject.GetComponent<Card>();
        cardObject.UpdateCardContent(card);
        newObject.transform.parent = content.transform;
        newObject.transform.localScale = new Vector3(1,1,1);
    }
}
