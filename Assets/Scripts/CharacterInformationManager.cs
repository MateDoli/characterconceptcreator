﻿
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class CharacterInformationManager : MonoBehaviour
{
    // [SerializeField] private InfoSO _infoSo;
    [SerializeField] private GameObject contentItem;
    [Header(" ")]
    [SerializeField] private TextMeshProUGUI className;

    [SerializeField] private Image classImage;
    [FormerlySerializedAs("description")] [SerializeField] private TextMeshProUGUI classDescription;
    [SerializeField] private TextMeshProUGUI raceDescription;
    [SerializeField] private TextMeshProUGUI proficiencies;

    [SerializeField] private Transform imageContainer;
    [SerializeField] private ScrollRect scrollRect;

    private void AddImages()
    {
        // foreach (var sprite in _infoSo.classImageArray)
        // {
        //     var newItem = Instantiate(contentItem, imageContainer);
        //     newItem.GetComponent<Image>().sprite = sprite;
        // }
    }

    private void OnDisable()
    {
        var images =imageContainer.transform.GetComponentsInChildren<Image>();
        if(images.Length <= 0) return;
        scrollRect.normalizedPosition = Vector2.up;
        foreach (var image in images)
        {
            Destroy(image);
        }
    }
    
    public void SetAndUpdateData(ChosenInfoData chosenInfoData)
    {
        CharacterInfoDataItem characterInfoDataItem = new CharacterInfoDataItem(chosenInfoData);
        
        classDescription.text = chosenInfoData.CharacterClass.ToString() + "\n" + characterInfoDataItem.ClassDescription;
        raceDescription.text = chosenInfoData.CharacterRace.ToString() + "\n" + characterInfoDataItem.RaceDescription;
        className.text = characterInfoDataItem.CharacterName;
        classImage.sprite = characterInfoDataItem.Sprite;
        proficiencies.text = "";
        var newItem = Instantiate(contentItem, imageContainer);
        newItem.GetComponent<Image>().sprite = characterInfoDataItem.Sprite;
    }
}

internal readonly struct CharacterInfoDataItem
{
    public string CharacterName { get; }
    public string RaceDescription { get; }
    public string ClassDescription { get; }
    public string ClassSkills { get; }
    // public string ClassTools { get; }
    public Sprite Sprite { get; }

    public CharacterInfoDataItem(ChosenInfoData chosenInfoData)
    {
        InfoLibrary library = InfoLibrary.instance;
        RaceDescription =  library.GetRaceDescription(chosenInfoData.CharacterRace);
        ClassDescription = library.GetClassInformation(chosenInfoData.CharacterClass).description;
        ClassSkills = library.GetClassInformation(chosenInfoData.CharacterClass).skills;
        // ClassTools = library.GetClassInformation(chosenInfoData.CharacterClass).tools;
        Sprite = library.GetImage(chosenInfoData.CharacterRace, chosenInfoData.CharacterClass);
        CharacterName = chosenInfoData.CharacterRace.ToString() + " " + chosenInfoData.CharacterClass.ToString();
    }
}

