
using UnityEditor;

public class ClassMenu : CardMenu
{

    public void UpdateMenuContent(CardSO raceCard)
    {
        base.UpdateMenuContent();
        foreach (var card in cards)
        {
            if (raceCard.classes.Contains(card.currentClass)) SpawnCard(card); 
        }
    }
}
