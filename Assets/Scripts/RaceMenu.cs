
public class RaceMenu : CardMenu
{
    public override void UpdateMenuContent()
    {
        base.UpdateMenuContent();
        foreach (var card in cards)
        {
            SpawnCard(card);
        }
    }
}
